window.addEventListener('load', function () { 

    NodeList.prototype.forEach = Array.prototype.forEach;

    var images = ['image-1-for-slider.png', 'image-2-for-slider.png'];
    var leftArrow = document.querySelector('.js-slider__left-arrow');
    var rightArrow = document.querySelector('.js-slider__right-arrow');
    var imageWrapper = document.querySelector('.js-slider__image-wrapper');
    var breadcrumbsCategory = document.querySelector('.breadcrumbs__category');
    var categories = document.querySelector('.categories');
    var centerblock = document.querySelector('.centerblock');
    var feedbackLink = document.querySelector('.feedback-link');
    var modal = document.querySelector('.modal');
    var closeModal = document.querySelector('.modal__close');
    var formInputs = document.querySelectorAll('.feedback-form__input');
    var submitButton = document.querySelector('.feedback-form__submit');

    var imageIndex = 0;
    var data = {"Автомобили": {}, 
                      "Бытовая техника": {"Микроволновки": [
                        {id: 1, price: "1900", description: "Микроволновка красная, купить сегодня нельзя завтра!", image: "img/product.png"},
                        {id: 2, price: "1900", description: "Микроволновка зеленая, купить сегодня нельзя завтра!", image: "img/product.png"},
                        {id: 3, price: "1900", description: "Микроволновка синяя, купить сегодня нельзя завтра!", image: "img/product.png"},
                        {id: 4, price: "1900", description: "Микроволновка оранжевая, купить сегодня нельзя завтра!", image: "img/product.png"},
                        {id: 5, price: "1900", description: "Микроволновка черная, купить сегодня нельзя завтра!", image: "img/product.png"}
                       ], "Холодильники": [
                        {id: 6, price: "1900", description: "Холодильник красный, купить сегодня нельзя завтра!", image: "img/product.png"},
                        {id: 7, price: "1900", description: "Холодильник зеленая, купить сегодня нельзя завтра!", image: "img/product.png"},
                        {id: 8, price: "1900", description: "Холодильник синяя, купить сегодня нельзя завтра!", image: "img/product.png"},
                        {id: 9, price: "1900", description: "Холодильник оранжевая, купить сегодня нельзя завтра!", image: "img/product.png"},
                        {id: 10, price: "1900", description: "Холодильник черная, купить сегодня нельзя завтра!", image: "img/product.png"}
                       ], "Посудомоечные машины": [
                        {id: 11, price: "1900", description: "Посудомоечая машина красная, купить сегодня нельзя завтра!", image: "img/product.png"},
                        {id: 12, price: "1900", description: "Посудомоечая машина зеленая, купить сегодня нельзя завтра!", image: "img/product.png"},
                        {id: 13, price: "1900", description: "Посудомоечая машина синяя, купить сегодня нельзя завтра!", image: "img/product.png"},
                        {id: 14, price: "1900", description: "Посудомоечая машина оранжевая, купить сегодня нельзя завтра!", image: "img/product.png"},
                        {id: 15, price: "1900", description: "Посудомоечая машина черная, купить сегодня нельзя завтра!", image: "img/product.png"}
                       ], "Чайники": [
                        {id: 11, price: "1900", description: "Чайник красная, купить сегодня нельзя завтра!", image: "img/product.png"},
                        {id: 12, price: "1900", description: "Чайник зеленая, купить сегодня нельзя завтра!", image: "img/product.png"},
                        {id: 13, price: "1900", description: "Чайник синяя, купить сегодня нельзя завтра!", image: "img/product.png"},
                        {id: 14, price: "1900", description: "Чайник оранжевая, купить сегодня нельзя завтра!", image: "img/product.png"},
                        {id: 15, price: "1900", description: "Чайник машина черная, купить сегодня нельзя завтра!", image: "img/product.png"}
                       ]},
                      "Мобильная техника": {},
                      "Компьютеры": {},
                      "Одежда": {},
                      "Недвижимость": {}};

    
    // Initial setup

    imageWrapper.style['background-image'] = "url('img/" + images[0] + "')";

    renderCategories();

    renderAllProducts(data)

    //Slider

    leftArrow.addEventListener('click', () => {
        imageIndex = imageIndex > 0 ? imageIndex - 1: images.length - 1;
        imageWrapper.style['background-image'] = "url('img/" + images[imageIndex] + "')";
    });

    rightArrow.addEventListener('click', () => {
        imageIndex = imageIndex == images.length - 1 ? 0 : imageIndex + 1;
        imageWrapper.style['background-image'] = "url('img/" + images[imageIndex] + "')";
    });


    setInterval(changeSliderImage, 5000);

    function changeSliderImage() {
        imageIndex = imageIndex == images.length - 1 ? 0 : imageIndex + 1;
        imageWrapper.style['background-image'] = "url('img/" + images[imageIndex] + "')";
    }


    //Catalogue

    function renderCategories() {
        var categoriesData = Object.keys(data);
        categories.innerHTML = "";

        categoriesData.forEach(el => {
            var subcategories;
            var subcatStr = '';

            subcategories = Object.keys(data[el]);
            
            subcategories.forEach(cat => {
                subcatStr += "<span class='categories__bullet'>&bull;</span><li class='categories__subcategory' data-category=" +
                                el.split(' ').join('-') + " data-subcategory=" + cat.split(' ').join('-') + ">" + cat + "</li></br>";
            });

            if(subcategories) {
                categories.innerHTML += "<li class='categories__item'><a href='#' class='categories__link' data-category=" +
                el.split(' ').join('-') + ">" + el + "</a>" + 
                "<ul class=categories__subcategories-list>" + subcatStr + "</ul>" +
                "</li>";
            }

           else {
            categories.innerHTML += "<li class='categories__item'><a href='#' class='categories__link'>" + 
                                        el + "</a></li>";
           }
        });
        
    }

    
    categories.addEventListener('click', (e) => {
        if(e.target.classList.contains('categories__subcategory')) {
            
            removeActiveFromSubcategories(e.target);

            e.target.classList.add('categories__subcategory--active');

            renderBySubcategory(e.target.dataset.category.split('-').join(' '), e.target.dataset.subcategory.split('-').join(' '));
            addPagination();
        }

        else if(e.target.classList.contains('categories__link')) {

            removeActivFromCategories();
            
            //add --active to categories__link and subcategories-list
            

            e.target.classList.add('categories__link--active');
            breadcrumbsCategory.innerHTML = e.target.innerHTML; //catregory shows up in breadcrumbs

            if(e.target.parentNode.querySelector('.categories__subcategories-list')) {
                e.target.parentNode.querySelector('.categories__subcategories-list').classList.add('categories__subcategories-list--active');
            }

            //render products from clicked category

            centerblock.innerHTML = '';

            renderByCategory(e.target.dataset.category.split('-').join(' '));
            addPagination();

            e.preventDefault();

        }
        
    });

    function removeActivFromCategories() {

        var catList = categories.querySelectorAll('.categories__link--active');

        if(catList) {
            catList.forEach(el => {
                el.classList.remove('categories__link--active');
                if(el.parentNode.querySelector('.categories__subcategories-list--active')) {
                    el.parentNode.querySelector('.categories__subcategories-list--active').classList.remove('categories__subcategories-list--active');
                }
            });
        }
    }

    function removeActiveFromSubcategories(element) {

        var subCatList = categories.querySelectorAll('.categories__subcategory--active');

        if(subCatList) subCatList.forEach(el => el.classList.remove('categories__subcategory--active'));
    }

    function renderAllProducts(data) {
        for(var category in data) {
                renderByCategory(category);
        }

        addPagination();
    }

    function renderByCategory(category) {

        if(category) {

            for(var subcategory in data[category]) {
                renderProduct(data[category][subcategory]);
            }

        }  
    }

    function renderBySubcategory(category, subcategory) {
        if(category && subcategory) {
            centerblock.innerHTML = '';

            renderProduct(data[category][subcategory]);
        }
    }

    function renderProduct(list) {


        list.forEach(product => {
            
            centerblock.innerHTML += "<div class='product centerblock__item' data-productId=" + product.id + ">" +

                                        "<div class='product__image-wrapper'>" +
                                            "<img class='product__image' src=" + product.image + ">" +
                                        "</div>" +

                                        "<p class='product__description'>" + 
                                            product.description +
                                        "</p>" +

                                        "<div class='product__price'>" + 
                                            product.price + " р." +
                                        "</div>" + 

                                        "<img class='product__basket' src='img/basket.png'>" + 
                                    "</div>"

        });
    }

    function addPagination() {
        var products = centerblock.querySelectorAll('.product');

        var amount = products.length;

        var pagesNumber = Math.ceil(amount / 6);

        if(pagesNumber < 2) return; 

        var pagination = document.createElement('DIV');
        pagination.className = 'pagination centerblock__pagination';
        pagination.innerHTML = '';

        for(var n=1; n < pagesNumber+1; n++) {
            pagination.innerHTML += "<a href='#' class='pagination__link pag-" + n +"'>" + n + "</a>";
        }

        centerblock.appendChild(pagination);
        

        for(let i=0; i < amount; i++) {
            let number = Math.ceil((i + 1)/6);
            products[i].classList.add("pag-" + number);
        }


        var pages = pagination.querySelectorAll('.pagination__link');

        //look for class on pagination link that represents page number and then find same class on products

        pages.forEach(page => {
            page.addEventListener('click', (e) => {

                var classes = page.className.split(' ');

                var number = classes.filter(el => el.startsWith("pag-"));

                number = number[0];

                products.forEach(product => {
                    if(product.classList.contains(number)) {
                        product.style.display = 'block';
                    }
                    else {
                        product.style.display = 'none';
                    }
                });

                pages.forEach(p => p.classList.remove('pagination__link--active'));

                page.classList.add('pagination__link--active');

                e.preventDefault();
                
            });            
        });

        //show first page when first loaded

        products.forEach((product, index) => {
            if(index > 5) product.style.display = 'none';
            pages[0].classList.add('pagination__link--active');
        });

        

    }

    //Modal feedback form

    feedbackLink.addEventListener('click', toggleModal);
    
    closeModal.addEventListener('click', toggleModal);       

    modal.addEventListener('click', e => {
        if(e.target == modal) toggleModal();
    });


    function toggleModal() {
        document.querySelector('.modal').classList.toggle('modal--show');
    }


   submitButton.addEventListener('click', (e) => {
       var formValid = true;

        formInputs.forEach(input => {
            formValid = checkValidity.call(input);
        });
        
        if(!formValid) e.preventDefault();
   });

   formInputs.forEach(input => {
        input.addEventListener('focusout', (e) => {
            checkValidity.call(input);
            input.addEventListener('input', checkValidity);    
       });
   });
         

    function checkValidity(e) {

        var input = e ? e.target : this;
        
        var error = input.nextElementSibling;

        var formIsValid = true;

        error.classList.remove('feedback-form__error--active');
        error.innerHTML = '';

        input.classList.remove('feedback-form__input--invalid');

        if(input.validity.valueMissing) {
            error.innerHTML = "Поле не должно быть пустым";
            formIsValid = false;
        }
        if(input.validity.typeMismatch && input.classList.contains('email')) {
            error.innerHTML = "Это не похоже на имейл!";
            formIsValid = false;
        }

        if(input.validity.patternMismatch && input.classList.contains('telephone')) {
            error.innerHTML = "Введите номер телефона в формате +380505555555";
            formIsValid = false;
        }

        if(!formIsValid) {
            error.classList.add('feedback-form__error--active');
            input.classList.add('feedback-form__input--invalid');
        }
        
        return formIsValid;
    }

});